/**
 * Represent Commando type soldier.
 * It extends Soldier class
 */
public class Commando extends Soldier {
    /**
     * Construction for commando with the specified name and position.
     * @param name is name of soldier
     * @param position is position of soldier.
     */
    public Commando(String name, Position position) { // DO NOT CHANGE PARAMETERS
        super(name, position, 10.0, 2.0, 10.0);
    }

    /**
     * This method is used for taking step.
     * According to state, object makes move.
     * If state is SEARCHING, if closest zombie is close enough, changes direction towards it and sets state to SHOOTING, else calculates position and if  closest zombie is close enough changes direction towards it and sets state to SHOOTING.
     * If state is SHOOTING, it creates bullet and if closest zombie is close enough, changes direction towards it, else sets random direction and sets state to SEARCHING.
     * @param controller is simulation controller
     */
    public void step(SimulationController controller) {
        Zombie closestZombie = null;
        double distance;

        if (this.getState() == SoldierState.SEARCHING) {
            closestZombie = this.findClosestZombie(controller);
            distance = findDistance(this.getPosition(), closestZombie);

            if (distance <= this.getShootingRange()) {
                this.changeDirectionTowards(closestZombie.getPosition());

                this.setState(SoldierState.SHOOTING);
            }

            else {
                this.calculateNextPosition(controller.getWidth(), controller.getHeight());

                closestZombie = this.findClosestZombie(controller);
                distance = findDistance(this.getPosition(), closestZombie);

                if (distance <= this.getShootingRange()) {
                    this.changeDirectionTowards(closestZombie.getPosition());

                    this.setState(SoldierState.SHOOTING);
                }
            }
        }

        else {
            createBullet(this.getPosition(), 40.0, this.getDirection(), controller);

            closestZombie = this.findClosestZombie(controller);
            distance = findDistance(this.getPosition(), closestZombie);

            if (distance <= this.getShootingRange()) {
                this.changeDirectionTowards(closestZombie.getPosition());
            }

            else {
                this.setDirection(Position.generateRandomDirection(true));

                this.setState(SoldierState.SEARCHING);
            }
        }
    }

}
