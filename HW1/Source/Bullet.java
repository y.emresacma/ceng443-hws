/**
 * Bullet class represent bullets and relevant method of it in simulation world
 * Bullet can live in simulation world just one step. I used active variable for it which we can access it from super class
 * There is a static variable to assign number to bullets stating from 0.
 */
public class Bullet extends SimulationObject {
    private static int count = 0;
    private final int bulletNumber;

    /***
     * This is a construction of bullet. Name, direction, speed position assigned here. Additionally, active variable sets to false
     * to make it inactive for 1 step.
     * @param position is object's position
     * @param speed is object's speed
     * @param direction is object's direction
     */
    public Bullet(Position position, double speed, Position direction) {
        super("bullet" + Integer.toString(count), position, speed);
        count++;
        bulletNumber = count;
        this.setDirection(direction);
        this.setActive(false);
    }

    /**
     * Calculates next bullet position with the current direction
     */
    public void calculateBulletPosition() {
        this.getPosition().add(this.getDirection());
    }

    /**
     * Returns closest zombie to object.
     * @param controller is simulation controller
     * @return Zombie
     */
    public Zombie findClosestZombie(SimulationController controller) {
        Zombie closestZombie = null;
        double closestDistance = controller.getHeight() + controller.getWidth();
        double distance;

        for (SimulationObject obj : controller.getAllObjects()) {
            distance = obj.getPosition().distance(this.getPosition());
            if (obj instanceof Zombie && closestDistance > distance && obj.isActive()) {
                closestZombie = (Zombie) obj;
                closestDistance = distance;
            }
        }

        return closestZombie;
    }

    /**
     * This method calculates what bullet is going to do.
     * It divides calculation according to it's speed.
     * At each step it check for collison, or whether moved out.
     * At the end, if nothing happens, bullet dropped out.
     * @param controller is simulation controller.
     */
    public void step(SimulationController controller) {
        boolean isBound = true;
        double x;
        double y;
        for (int i = 0; i < this.getSpeed(); i++) {
            Zombie closestZombie = this.findClosestZombie(controller);
            double distance = closestZombie.getPosition().distance(this.getPosition());

            if (distance <= closestZombie.getCollisionRange()){
                closestZombie.setActive(false);
                System.out.println(this.getName() + " hit " + closestZombie.getName() + ".");
                return;
            }

            this.calculateBulletPosition();

            x = this.getPosition().getX();
            y = this.getPosition().getY();

            if (0 <= x && x <= (controller.getWidth() - 1) && 0 <= y && y <= (controller.getHeight() - 1)) {
                isBound = true;
            }

            else {
                isBound = false;
                break;
            }
        }

        if (isBound) {
                System.out.println(this.getName() + " dropped to the ground at " + this.getPosition().toString() + ".");
        }

        else {
            System.out.println(this.getName() + " moved out of bounds.");
        }
    }
}

