/**
 * Represent regular zombie type Zombie.
 * It extends Zombie class
 */
public class RegularZombie extends Zombie {
    /**
     * Construction for regular zombie with the specified name and position.
     * @param name is zombie's name
     * @param position is zombie's position.
     */
    public RegularZombie(String name, Position position) { // DO NOT CHANGE PARAMETERS
        super(name, position, 5.0, 2.0, 20.0);
    }

    /**
     * This method is used for taking step.
     * According to state, object makes move.
     * It checks whether it can kill soldier or not. If it is exit from function.
     * If state is WANDERING, it calculates the position and if distance is close enough sets state to FOLLOWING.
     * If state is FOLLOWING, it calculates the position and if number of zombie which is in FOLLOWING state is 4, sets state to WANDERING.
     * @param controller is simulation controller
     */
    public void step(SimulationController controller) {
        boolean isKillingSoldier = this.isKillingSoldier(controller);

        if (isKillingSoldier) {
            return;
        }

        else {
            if (this.getState() == ZombieState.WANDERING) {
                this.calculateNextPosition(controller.getWidth(), controller.getHeight());

                Soldier closestSoldier = this.findClosestSoldier(controller);
                double distance = findDistance(this.getPosition(), closestSoldier);

                if (distance <= this.getDetectionRange()) {
                    this.setState(ZombieState.FOLLOWING);
                }
            }

            else {
                this.calculateNextPosition(controller.getWidth(), controller.getHeight());

                int number = 0;

                for (SimulationObject obj : controller.getAllObjects()) {
                    if (obj instanceof Zombie && ((Zombie) obj).getState() == ZombieState.FOLLOWING) {
                        number++;
                    }
                }

                if (number == 4) {
                    this.setState(ZombieState.WANDERING);
                }
            }
        }
    }
}
