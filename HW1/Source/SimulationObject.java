/**
 * Represent simulation object in simulation world such as soldier, zombie, bullet.
 * It has name, position, direction, speed and active variables.
 */
public abstract class SimulationObject {
    private final String name;
    private Position position;
    private Position direction;
    private final double speed;
    private boolean active;

    /**
     * Construction for Simulation object.
     * @param name is name of object
     * @param position is position of object
     * @param speed is speed of object
     */
    public SimulationObject(String name, Position position, double speed) {
        this.name = name;
        this.position = position;
        this.speed = speed;
        this.direction = null;
        this.active = true;
    }

    /**
     * Returns object's name.
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Returns object's position.
     * @return Position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Assigns position to object.
     * @param position is position which will be assigned to object
     */
    public void setPosition(Position position) {
        this.position = position;
        System.out.println(this.getName() + " moved to " + this.getPosition().toString() + ".");
    }

    /**
     * Returns object's direction.
     * @return Position
     */
    public Position getDirection() {
        return direction;
    }

    /**
     * Assigns direction to object.
     * If object is instance of bullet, print nothing.
     * @param direction is direction which will be assigned to object
     */
    public void setDirection(Position direction) {
        if(this instanceof Bullet) {
            this.direction = direction;
            return;
        }

        this.direction = direction;
        System.out.println(this.getName() + " changed direction to " + this.getDirection().toString() + ".");

    }

    /**
     * Returns object's speed.
     * @return double
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * Checks whether object is active or not.
     * @return boolean
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Assigns active variable to object.
     * @param active is boolean value which shows active state
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Changes object position if next position is in bound.
     * If not changes direction randomly.
     * @param width is width of simulation world
     * @param height is height of simulation world
     */
    public void calculateNextPosition(double width, double height) {
        double x = this.getPosition().getX() + this.getSpeed() * this.getDirection().getX();
        double y = this.getPosition().getY() + this.getSpeed() * this.getDirection().getY();

        if (0 <= x && x <= (width - 1) && 0 <= y && y <= (height - 1)) {
            this.setPosition(new Position(x, y));
        }

        else {
            this.setDirection(Position.generateRandomDirection(true));
        }
    }

    /**
     * Changes direction towards to given position.
     * @param position is which will be used to assign direction towards it
     */
    public void changeDirectionTowards(Position position) {
        double x = position.getX() - this.getPosition().getX();
        double y = position.getY() - this.getPosition().getY();

        Position direction = new Position(x, y);
        direction.normalize();

        this.setDirection(direction);
    }

    /**
     * Returns the distance with the given object and given position.
     * @param pos is given position
     * @param obj is given object
     * @return double
     */
    public double findDistance(Position pos, SimulationObject obj) {
        if (obj == null) {
            return Double.POSITIVE_INFINITY;
        }

        else {
            return pos.distance(obj.getPosition());
        }
    }

    public abstract void step(SimulationController controller);
}
