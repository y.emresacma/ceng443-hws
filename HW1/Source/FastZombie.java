/**
 * Represent fast zombie type Zombie.
 * It extends Zombie class
 */
public class FastZombie extends Zombie {
    /**
     * Construction for fast zombie with the specified name and position.
     * @param name is zombie's name
     * @param position is zombie's position
     */
    public FastZombie(String name, Position position) { // DO NOT CHANGE PARAMETERS
        super(name, position, 20.0, 2.0, 20.0);
    }

    /**
     * This method is used for taking step.
     * According to state, object makes move.
     * It checks whether it can kill soldier or not. If it is exit from function.
     * If state is WANDERING, if closest soldier is close enough, changes direction toward it and sets state to FOLLOWING, else calculates position.
     * If state is FOLLOWING, calculates position and sets state to WANDERING.
     * @param controller is simulation controller
     */
    public void step(SimulationController controller) {
        boolean isKilling = this.isKillingSoldier(controller);

        if (isKilling) {
            return;
        }

        else {
            if (this.getState() == ZombieState.WANDERING) {
                Soldier closestSoldier = this.findClosestSoldier(controller);
                double distance = findDistance(this.getPosition(), closestSoldier);

                if (distance <= this.getDetectionRange()) {
                    this.changeDirectionTowards(closestSoldier.getPosition());

                    this.setState(ZombieState.FOLLOWING);
                }

                else {
                    this.calculateNextPosition(controller.getWidth(), controller.getHeight());
                }
            }

            else {
                this.calculateNextPosition(controller.getWidth(), controller.getHeight());

                this.setState(ZombieState.WANDERING);
            }
        }
    }
}
