import java.util.ArrayList;

/**
 * Represents Simulation controller.
 * It controls whole simulation world.
 * It has height, width.
 * Additionally, it has 2 list, one of them consist of bullets, the other consist of soldiers and zombies.
 */
public class SimulationController {
    private final double height;
    private final double width;
    private ArrayList<SimulationObject> allObjects = new ArrayList<>();
    private ArrayList<SimulationObject> bullets = new ArrayList<>();
    private boolean initial = true;   /////**  USE FOR FİRST DİRECTİON ASSİGMENT

    /**
     * Construciton for simulation controller.
     * It sets width and height of simulation world.
     * @param width is width of simulation world
     * @param height is height of simulation world
     */
    public SimulationController(double width, double height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Returns height of simulation world.
     * @return double
     */
    public double getHeight() {
        return height;
    }

    /**
     * Returns width ofsimulation world.
     * @return double
     */
    public double getWidth() {
        return width;
    }

    /**
     * Returns all objects which are soldiers and zombies.
     * @return List
     */
    public ArrayList<SimulationObject>  getAllObjects() {
        return this.allObjects;
    }

    /**
     * Returns all bullets.
     * @return List
     */
    public ArrayList<SimulationObject>  getBullets() {
        return this.bullets;
    }

    /**
     * Add given bullet to the bullet list.
     * @param obj is simulation object
     */
    public void addBullet(SimulationObject obj) {
        this.bullets.add(obj);
    }

    /**
     * Removes given bullet from bullet list.
     * @param obj is simulation object which is bullet
     */
    public void removeBullet(SimulationObject obj) {
        this.bullets.remove(obj);
    }

    /**
     * Runs all simulation object's step function one by one.
     * At first call, it assigns direction to the objects.
     * For bullets, it checks whether the bullet active or not.
     * If it is active, calls step function and delete it.
     * Else, it makes bullet active.
     */
    public void stepAll() {
        if (initial) {
            for (SimulationObject obj : this.getAllObjects()) {
                obj.setDirection(Position.generateRandomDirection(true));
                if (obj.isActive()) {
                    obj.step(this);
                }

            }
            initial = false;
        }

        else {
            for (SimulationObject obj : this.getAllObjects()) {
                if (obj.isActive()) {
                    obj.step(this);
                }
            }

            for (SimulationObject obj : new ArrayList<SimulationObject>(this.getBullets())) {
                if (obj.isActive()) {
                    obj.step(this);
                    this.removeBullet(obj);
                }

                else {
                    obj.setActive(true);
                }
            }
        }
    }

    /**
     * Adds given simulation object to the simulation list.
     * @param obj is simulation object
     */
    public void addSimulationObject(SimulationObject obj) {
        allObjects.add(obj);
    }

    /**
     * Removes given simulation object from simulation object list.
     * @param obj is simulation object
     */
    public void removeSimulationObject(SimulationObject obj) {
        allObjects.remove(obj);
    }

    /**
     * Checks whether there exist just zombies, just soldier or none of them.
     * If it is, returns true.
     * Else, returns false
     * @return boolean
     */
    public boolean isFinished() {
        int zombie = 0;
        int soldier = 0;

        for (SimulationObject obj : this.getAllObjects()) {
            if (obj instanceof Zombie && obj.isActive()) {
                zombie++;
            }

            else if (obj instanceof Soldier && obj.isActive()) {
                soldier++;
            }
        }

        if (zombie * soldier > 0) {
            return false;
        }

        return true;
    }
}
