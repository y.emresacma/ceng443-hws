
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 *
 */
public class SimulationRunner {

    public static void main(String[] args) {
        SimulationController simulation = new SimulationController(50, 50);

        simulation.addSimulationObject(new RegularSoldier("Soldier1", new Position(15, 15)));
        simulation.addSimulationObject(new Sniper("Soldier3", new Position(35, 15)));

        simulation.addSimulationObject(new Commando("Soldier2", new Position(25, 15)));
        simulation.addSimulationObject(new SlowZombie("Zombie1", new Position(10, 0)));
        simulation.addSimulationObject(new RegularZombie("Zombie2", new Position(40, 0)));
        simulation.addSimulationObject(new FastZombie("Zombie3", new Position(30, 0)));

        simulation.getAllObjects().get(0).setDirection(Position.generateRandomDirection(true));

        while (!simulation.isFinished()) {
                simulation.stepAll();
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(SimulationRunner.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
