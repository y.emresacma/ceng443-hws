/**
 * Represents zombie in simulation world.
 * It has Zombie state, collision range and detection range additional to Simulation object.
 * It exteds Simulation object.
 */
public abstract class Zombie extends SimulationObject {
    private ZombieState state;
    private final double collisionRange;
    private final double detectionRange;

    /**
     * Construction for zombie object with the specified name, position, speed, collision range and detection range.
     * Zombie state is WANDERING initially
     * @param name is zombie's name
     * @param position is zombie's position
     * @param speed is zombie's speed
     * @param collisionRange is zombie's collision range
     * @param detectionRange is zombie's detection range
     */
    public Zombie(String name, Position position, double speed, double collisionRange, double detectionRange) {
        super(name, position, speed);
        this.collisionRange = collisionRange;
        this.detectionRange = detectionRange;
        this.state = ZombieState.WANDERING;
    }

    /**
     * Returns collision range of object.
     * @return double
     */
    public double getCollisionRange() {
        return this.collisionRange;
    }

    /**
     * Returns detection range of soldier.
     * @return double
     */
    public double getDetectionRange() {
        return this.detectionRange;
    }

    /**
     * Returns zombie state of object.
     * @return ZombieState
     */
    public ZombieState getState() {
        return this.state;
    }

    /**
     * Sets zombie state of object
     * @param state is zombie state which will be assigned
     */
    public void setState(ZombieState state) {
        this.state = state;
        System.out.println(this.getName() + " changed state to " + this.getState().toString() + ".");
    }

    /**
     * Returns closest soldier in simulation world.
     * @param controller is simulation controller
     * @return Soldier
     */
    public Soldier findClosestSoldier(SimulationController controller) {
        Soldier closestSoldier = null;
        double closestDistance = controller.getHeight() + controller.getWidth();
        double distance;

        for (SimulationObject obj : controller.getAllObjects()) {
            distance = obj.getPosition().distance(this.getPosition());
            if (obj instanceof Soldier && closestDistance > distance && obj.isActive()) {
                closestSoldier = (Soldier) obj;
                closestDistance = distance;
            }
        }

        return closestSoldier;
    }

    /**
     * Checks whether object kills the closest soldier or not.
     * If it is, object kills it.
     * @param controller i simulation controller
     * @return boolean
     */
    public boolean isKillingSoldier(SimulationController controller) {
        Soldier closestSoldier = this.findClosestSoldier(controller);
        double distance = findDistance(this.getPosition(), closestSoldier);

        if (closestSoldier != null && distance <= (this.getCollisionRange() + closestSoldier.getCollisionRange())) {
            closestSoldier.setActive(false);
            System.out.println(this.getName() + " killed " + closestSoldier.getName() + " ");

            return true;
        }

        return false;
    }
}
