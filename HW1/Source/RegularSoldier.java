/**
 * Represent regular soldier type soldier.
 * It extends Soldier class
 */
public class RegularSoldier extends Soldier {
    /**
     * Construction for regular soldier with the specified name and position.
     * @param name is soldier's name
     * @param position is soldier's position
     */
    public RegularSoldier(String name, Position position) { // DO NOT CHANGE PARAMETERS
        super(name, position, 5.0, 2.0, 20.0);
    }

    /**
     * This method is used for taking step.
     * According to state, object makes move.
     * If state is SEARCHING, calculates the position and if closest zombie is close enough sets state to AIMING.
     * If state is AIMING, if closest zombie is close enough, changes direction towards it and sets state to SHOOTING, else sets state to SEARCHING
     * If state is SHOOTING, it creates a bullet, if closest zombie is close enough sets state to AIMING, else changes direction randomly and sets state to SEARCHING.
     * @param controller is simulation controller
     */
    public void step(SimulationController controller) {
        Zombie closestZombie;
        double distance;

        if (this.getState() == SoldierState.SEARCHING) {
            this.calculateNextPosition(controller.getWidth(), controller.getHeight());

            closestZombie = this.findClosestZombie(controller);
            distance = findDistance(this.getPosition(), closestZombie);

            if (distance <= this.getShootingRange()) {
                this.setState(SoldierState.AIMING);
            }
        }

        else if (this.getState() == SoldierState.AIMING) {
            closestZombie = this.findClosestZombie(controller);
            distance = findDistance(this.getPosition(), closestZombie);

            if (distance <= this.getShootingRange()) {
                this.changeDirectionTowards(closestZombie.getPosition());

                this.setState(SoldierState.SHOOTING);
            }

            else {
                this.setState(SoldierState.SEARCHING);
            }
        }

        else {
            createBullet(this.getPosition(), 40.0, this.getDirection(), controller);

            closestZombie = this.findClosestZombie(controller);
            distance = findDistance(this.getPosition(), closestZombie);

            if (distance <= this.getShootingRange()) {
                this.setState(SoldierState.AIMING);
            }

            else {
                this.setDirection(Position.generateRandomDirection(true));

                this.setState(SoldierState.SEARCHING);
            }
        }
    }

}
