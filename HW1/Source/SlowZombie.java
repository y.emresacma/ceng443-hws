/**
 * Represent slow zombie type Zombie.
 * It extends Zombie class
 */
public class SlowZombie extends Zombie {
    /**
     * Construction for slow zombie with the specified name and position.
     * @param name is zombie's name
     * @param position is zombie'S position
     */
    public SlowZombie(String name, Position position) { // DO NOT CHANGE PARAMETERS
        super(name, position, 2.0, 1.0, 40.0);
    }

    /**
     * This method is used for taking step.
     * According to state, object makes move.
     * It checks whether it can kill soldier or not. If it is exit from function.
     * If state is wandering, if closest soldier is close enough sets state to FOLLOWING, else calculates next position.
     * If state is FOLLOWING, if closest soldier is close enough sets state to WANDERING and change direction towards soldier then calculates the position, else just calculates the position
     * @param controller is simulation controller
     */
    public void step(SimulationController controller) {
        boolean isKillingSoldier = this.isKillingSoldier(controller);

        if (isKillingSoldier) {
            return;
        }

        else {
            Soldier closestSoldier = this.findClosestSoldier(controller);
            double distance = findDistance(this.getPosition(), closestSoldier);

            if (this.getState() == ZombieState.WANDERING) {
                if (distance <= this.getDetectionRange()) {
                    this.setState(ZombieState.FOLLOWING);
                }

                else {
                    this.calculateNextPosition(controller.getWidth(), controller.getHeight());
                }
            }

            else {
                if (distance <= this.getDetectionRange()) {
                    this.changeDirectionTowards(closestSoldier.getPosition());

                    this.setState(ZombieState.WANDERING);
                }

                this.calculateNextPosition(controller.getWidth(), controller.getHeight());
            }
        }
    }
}
