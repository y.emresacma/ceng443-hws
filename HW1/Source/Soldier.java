/**
 * Represents soldier in simulation world.
 * It has Soldier state, collision range and shooting range additional to Simulation object.
 * It exteds Simulation object.
 */
public abstract class Soldier extends SimulationObject {
    private SoldierState state;
    private final double collisionRange;
    private final double shootingRange;

    /**
     * Construction for soldier object with the specified name, position, speed, collision range and shooting range.
     * Soldier state is SEARCHING initially.
     * @param name is soldier's name
     * @param position is soldier's position
     * @param speed is soldier's speed
     * @param collisionRange is soldier's collision range
     * @param shootingRange is soldier's shooting range
     */
    public Soldier(String name, Position position, double speed, double collisionRange, double shootingRange) {
        super(name, position, speed);
        this.collisionRange = collisionRange;
        this.shootingRange = shootingRange;
        this.state = SoldierState.SEARCHING;
    }

    /**
     * Returns soldier state of object
     * @return SoldierState
     */
    public SoldierState getState() {
        return this.state;
    }

    /**
     * Sets soldier state to object
     * @param state is soldier state which will be assigned
     */
    public void setState(SoldierState state) {
        this.state = state;
        System.out.println(this.getName() + " changed state to " + this.getState().toString() + ".");
    }

    /**
     * Returns collision range of object.
     * @return double
     */
    public double getCollisionRange() {
        return this.collisionRange;
    }

    /**
     * Returns shooting range of object.
     * @return double
     */
    public double getShootingRange() {
        return this.shootingRange;
    }

    /**
     * It creates bullet and adds to the simulation world using given parameters.
     * @param position is bullet's position
     * @param speed is bullet's speed
     * @param direction is bullet's direction
     * @param controller is simulation controller.
     */
    public void createBullet(Position position, double speed, Position direction, SimulationController controller) {
        Bullet bullet = new Bullet(position, speed, direction);
        controller.addBullet(bullet);
        System.out.println(this.getName() + " fired " + bullet.getName() + " to " + direction.toString() + ".");
    }

    /**
     * Returns closest zombie to object.
     * @param controller is simulation controller
     * @return Zombie
     */
    public Zombie findClosestZombie(SimulationController controller) {
        Zombie closestZombie = null;
        double closestDistance = controller.getHeight() + controller.getWidth();
        double distance;

        for (SimulationObject obj : controller.getAllObjects()) {
            distance = obj.getPosition().distance(this.getPosition());
            if (obj instanceof Zombie && closestDistance > distance && obj.isActive()) {
                closestZombie = (Zombie) obj;
                closestDistance = distance;
            }
        }

        return closestZombie;
    }
}
