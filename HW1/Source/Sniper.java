/**
 * Represent sniper type soldier.
 * It extends Soldier class
 */
public class Sniper extends Soldier {
    /**
     * Construction for sniper with the specified name and position.
     * @param name is soldier's name
     * @param position is soldier's position
     */
    public Sniper(String name, Position position) { // DO NOT CHANGE PARAMETERS
        super(name, position, 2.0, 5.0, 40.0);
    }

    /**
     * This method is used for taking step.
     * According to state, object makes move.
     * If state is SEARCHING, it calculates next position and sets the state to AIMING.
     * If state is AIMING, it calculates closest zombie, ıf it close enough sets state to SHOOTING, else set state to SEARCHING.
     * If state is SHOOTING, it creates bullet and if closest zombie is close enough  sets state to AIMING, else sets state to SEARCHING.
     * @param controller is simulation controller
     */
    public void step(SimulationController controller) {
        Zombie closestZombie = null;
        double distance;

        if (this.getState() == SoldierState.SEARCHING) {
            this.calculateNextPosition(controller.getWidth(), controller.getHeight());

            this.setState(SoldierState.AIMING);
        }

        else if (this.getState() == SoldierState.AIMING) {
            closestZombie = this.findClosestZombie(controller);
            distance = findDistance(this.getPosition(), closestZombie);

            if (distance <= this.getShootingRange()) {
                this.changeDirectionTowards(closestZombie.getPosition());

                this.setState(SoldierState.SHOOTING);
            }

            else {
                this.setState(SoldierState.SEARCHING);
            }
        }

        else {
            createBullet(this.getPosition(), 100.0, this.getDirection(), controller);

            closestZombie = this.findClosestZombie(controller);
            distance = findDistance(this.getPosition(), closestZombie);

            if (distance <= this.getShootingRange()) {
                this.setState(SoldierState.AIMING);
            }

            else {
                this.setDirection(Position.generateRandomDirection(true));

                this.setState(SoldierState.SEARCHING);
            }
        }
    }

}
